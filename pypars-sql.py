__author__ = 'krishnateja'

#
from pyparsing import Literal, CaselessLiteral, Word, Upcase, delimitedList, Optional, \
    Combine, Group, alphas, nums, alphanums, ParseException, Forward, oneOf, quotedString, \
    ZeroOrMore, restOfLine, Keyword, Each


def test(str):
    print str, "->"
    try:
        tokens = simpleSQL.parseString(str)
        print "tokens = ", tokens
        print "First 1 in columns:", tokens.columns[0]
        print "tokens.columns =", tokens.columns
        print "tokens.tables =", tokens.tables
        print "tokens.where =", tokens.where
        print "tokens.metrics =", tokens.metrics
        print "tokens.groupby =", tokens.groupby
        print "tokens.orderby =", tokens.orderby
    except ParseException, err:
        print " " * err.loc + "^\n" + err.msg
        print err
    print

# define SQL tokens
selectStmt = Forward()
selectToken = Keyword("select", caseless=True)
fromToken = Keyword("from", caseless=True)
sumToken = Keyword("sum", caseless=True)
countToken = Keyword("count", caseless=True)

ident = Word(alphas, alphanums + "_$()-*").setName("identifier")
columnName = delimitedList(ident, ".", combine=True)
columnNameList = Group(delimitedList(columnName))
tableName = delimitedList(ident, ".", combine=True)
tableNameList = Group(delimitedList(tableName))

whereExpression = Forward()
selectExpression = Forward()
sum_count_Expression = Forward()

and_ = Keyword("and", caseless=True)
or_ = Keyword("or", caseless=True)
in_ = Keyword("in", caseless=True)
as_ = Keyword("as", caseless=True)
orderby = Keyword("order by", caseless=True)
groupby = Keyword("group by", caseless=True)

columns = Group(delimitedList(ident))
columnVal = (nums | quotedString)

E = CaselessLiteral("E")
binop = oneOf("= != < > >= <= eq ne lt le gt ge", caseless=True)
arithSign = Word("+-", exact=1)
realNum = Combine(Optional(arithSign) + (Word(nums) + "." + Optional(Word(nums)) |
                                         ("." + Word(nums))) +
                  Optional(E + Optional(arithSign) + Word(nums)))
intNum = Combine(Optional(arithSign) + Word(nums) +
                 Optional(E + Optional("+") + Word(nums)))

columnRval = realNum | intNum | quotedString | columnName  # need to add support for alg expressions
whereCondition = Group(
    (columnName + binop + columnRval) |
    (columnName + in_ + "(" + delimitedList(columnRval) + ")") |
    (columnName + in_ + "(" + selectStmt + ")") |
    ("(" + whereExpression + ")")
)
whereExpression << whereCondition + ZeroOrMore((and_ | or_) + whereExpression)
selectExpression << columnNameList + ZeroOrMore(as_ + ident)
sum_count_Expression << ZeroOrMore((sumToken | countToken) + "(" + ("*" | ident) + ")") + ZeroOrMore(as_ + ident)

# define the grammar
selectStmt << (selectToken +
               ('*' | selectExpression).setResultsName("columns") +
               fromToken +
               tableNameList.setResultsName("tables") +
               Optional(Group(CaselessLiteral("where") + whereExpression), "").setResultsName("where") +
               Each([Optional(groupby + columns("groupby"), '').setDebug(False),
                     Optional(orderby + columns("orderby"), '').setDebug(False)
                     ])
               )

simpleSQL = selectStmt

# define Oracle comment format, and ignore them
oracleSqlComment = "--" + restOfLine
simpleSQL.ignore(oracleSqlComment)
test("SELECT country_name, region, country_code, COUNT(*) FROM wb_health_population WHERE year >= '2006-01-13T19:09:19.656473' AND year <= '2016-01-14T19:09:19' GROUP BY country_name, region, country_code ORDER BY COUNT(*) DESC LIMIT 50000 OFFSET 0")
test("SELECT * from XYZZY, ABC group by city, school order by firstname, lastname")
test("select * from SYS.XYZZY")
test("Select A as a from Sys.dual")
test("Select A,B,C from Sys.dual")
test("Select A, B, C as c from Sys.dual")
test("Select A as a, B, C from Sys.dual, Table2   ")
test("Xelect A, B, C from Sys.dual")
test("Select A, B, C frox Sys.dual")
test("Select")
test("Select &&& frox Sys.dual")
test("Select A from Sys.dual where a in ('RED','GREEN','BLUE')")
test("Select A from Sys.dual where a in ('RED','GREEN','BLUE') and b in (10,20,30)")
test("Select A,b from table1,table2 where table1.id eq table2.id -- test out comparison operators")
test("Select A,b from table1,table2 where table1.id eq table2.id -- test out comparison operators")
test("SELECT gender, state, name, SUM(num) FROM birth_names WHERE ds >= '1966-01-13T14:31:48.901166' AND ds <= '2016-01-14T14:31:48' GROUP BY gender, state, name ORDER BY SUM(num) DESCLIMIT 50000 OFFSET 0")